set(CTEST_USE_LAUNCHERS "ON" CACHE STRING "")

set(AEVA_ENABLE_TESTING "ON" CACHE BOOL "")

# Build binaries that will run on older architectures
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "fedora")
  set(CMAKE_C_FLAGS "-march=core2 -mno-avx512f" CACHE STRING "")
  set(CMAKE_CXX_FLAGS "-march=core2 -mno-avx512f" CACHE STRING "")
endif ()

include("${CMAKE_CURRENT_LIST_DIR}/configure_sccache.cmake")

# Avoid confusing the interface include directories detection logic.
set(CMAKE_INSTALL_PREFIX "$ENV{CI_BUILDS_DIR}/install-prefix" CACHE PATH "")

# Include the superbuild settings.
include("$ENV{SUPERBUILD_PREFIX}/aevasession-developer-config.cmake")
