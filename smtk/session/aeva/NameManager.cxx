//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/NameManager.h"

#include "smtk/model/Entity.h"

#include "smtk/resource/PersistentObject.h"

#include <sstream>

namespace smtk
{
namespace session
{
namespace aeva
{

NameManager::NameManager()
  : m_counter(0)
{
}

NameManager::Ptr NameManager::create()
{
  return std::make_shared<NameManager>();
}

std::string NameManager::nameForObject(const smtk::resource::PersistentObject& obj)
{
  std::string objType;
  const auto* entity = dynamic_cast<const smtk::model::Entity*>(&obj);
  if (entity)
  {
    objType = entity->flagSummary();
  }
  if (objType.empty())
  {
    auto typeName = obj.typeName();
    std::size_t cut = typeName.rfind("::");
    if (cut != std::string::npos)
    {
      objType = typeName.substr(cut);
    }
    else
    {
      objType = typeName;
    }
  }

  std::ostringstream ns;
  ns << objType << " " << m_counter++;
  return ns.str();
}

}
}
}
