//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_Geometry_h
#define smtk_session_aeva_Geometry_h

#include "smtk/session/aeva/Exports.h"

#include "smtk/extension/vtk/geometry/Geometry.h"

#include "smtk/geometry/Cache.h"

#include "smtk/PublicPointerDefs.h"

namespace smtk
{
namespace session
{
namespace aeva
{

class Resource;

/**\brief A VTK geometry provider for the aeva session.
  *
  */
class SMTKAEVASESSION_EXPORT Geometry
  : public smtk::geometry::Cache<smtk::extension::vtk::geometry::Geometry>
{
public:
  using CacheBaseType = smtk::extension::vtk::geometry::Geometry;
  smtkTypeMacro(smtk::session::aeva::Geometry);
  smtkSuperclassMacro(smtk::geometry::Cache<CacheBaseType>);
  using DataType = Superclass::DataType;

  Geometry(const std::shared_ptr<smtk::session::aeva::Resource>& parent);
  virtual ~Geometry() = default;

  smtk::geometry::Resource::Ptr resource() const override;
  void queryGeometry(const smtk::resource::PersistentObject::Ptr& obj,
    CacheEntry& entry) const override;
  int dimension(const smtk::resource::PersistentObject::Ptr& obj) const override;
  Purpose purpose(const smtk::resource::PersistentObject::Ptr& obj) const override;
  void update() const override;

  void geometricBounds(const DataType&, BoundingBox& bbox) const override;

protected:
  std::weak_ptr<smtk::session::aeva::Resource> m_parent;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif // smtk_session_aeva_Geometry_h
