//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_pqAEVASurfaceFeatureToolBar_h
#define smtk_session_aeva_pqAEVASurfaceFeatureToolBar_h

#include "smtk/PublicPointerDefs.h"

#include <QToolBar>

class vtkSMSMTKWrapperProxy;
class pqSMTKWrapper;
class pqServer;

namespace smtk
{
namespace session
{
namespace aeva
{
class CellSelection;
}
}
}

/**\brief Toolbar buttons to turn selections into surface features.
  *
  */
class pqAEVASurfaceFeatureToolBar : public QToolBar
{
  Q_OBJECT
  using Superclass = QToolBar;

public:
  pqAEVASurfaceFeatureToolBar(QWidget* parent = nullptr);
  ~pqAEVASurfaceFeatureToolBar() override;

  /// There should be only one toolbar. Return the singleton instance.
  static pqAEVASurfaceFeatureToolBar* instance();

  /// externally trigger the Deselect toolbar button - used by pqAEVAShortcuts.
  static void activateDeselect();

  /// externally trigger the Duplicate toolbar button - used by pqAEVAShortcuts.
  void activateDuplicate();

protected slots:
  /// Called whenever a new client-server connection is made and SMTK is enabled on the server.
  ///
  /// This is used to observe when the selection changes.
  virtual void observeWrapper(pqSMTKWrapper* wrapper, pqServer* server);
  /// Called whenever a client-server disconnect is performed on an SMTK-enabled server.
  virtual void unobserveWrapper(pqSMTKWrapper* wrapper, pqServer* server);

protected:
  /// Called when the selection changes.
  virtual void onSelectionChanged(const std::string& selnSource,
    const std::shared_ptr<smtk::view::Selection>& seln);
  /// Called with the primitive selection included in a selection (or null if none).
  ///
  /// NB: This assumes only one CellSelection exists at a time.
  virtual void onPrimitivesSelected(smtk::session::aeva::CellSelection* cellSeln);

  class pqInternal;
  pqInternal* m_p;

private:
  Q_DISABLE_COPY(pqAEVASurfaceFeatureToolBar);
};

#endif
