//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_ContextMenu_h
#define smtk_session_aeva_ContextMenu_h

#include "pqContextMenuInterface.h"
#include "smtk/session/aeva/Resource.h"

#include <QAction>
#include <QMenu>
#include <QPointer>

class pqDataRepresentation;
class pqSMTKResourceRepresentation;
class vtkPVCompositeRepresentation;
class vtkSMTKResourceRepresentation;

/**\brief AEVAContextMenu
 *
 * AEVAContextMenu replaces ParaView's context menu with one specific to aeva.
 */
class AEVAContextMenu
  : public QObject
  , public pqContextMenuInterface
{
  Q_OBJECT
  Q_INTERFACES(pqContextMenuInterface)
public:
  AEVAContextMenu();
  AEVAContextMenu(QObject* parent);
  virtual ~AEVAContextMenu() = default;

  bool contextMenu(QMenu* menu,
    pqView* viewContext,
    const QPoint& viewPoint,
    pqRepresentation* dataContext,
    const QList<unsigned int>& dataBlockContext) const override;

  // Use a higher priority than the default menu, which we will eliminate
  // since some of its controls (block properties) will be ignored.
  int priority() const override { return 1024; }

public slots:
  virtual void hide();
  virtual void hideOthers();

  virtual void changeRepresentationType(QAction* action);

protected:
  template<class OpType>
  QAction* addOperation(QMenu* menu, const std::string& label) const;
  QAction* addOperation(const std::string& opType, QMenu* menu, const std::string& label) const;

  void operationDialog(const std::shared_ptr<smtk::operation::Operation>& operation) const;

  mutable QPointer<pqDataRepresentation> PickedRep;
  mutable QPointer<pqSMTKResourceRepresentation> SMTKRep;
  mutable vtkSMTKResourceRepresentation* ResourceRep;
  mutable vtkPVCompositeRepresentation* CompositeRep;
  mutable std::shared_ptr<smtk::session::aeva::Resource> Resource;
  mutable std::vector<std::shared_ptr<smtk::resource::Component> > ContextItems;

private:
  Q_DISABLE_COPY(AEVAContextMenu)
};

template<class OpType>
QAction* AEVAContextMenu::addOperation(QMenu* menu, const std::string& label) const
{
  std::string opType(OpType::type_name);
  return this->addOperation(opType, menu, label);
}

#endif
