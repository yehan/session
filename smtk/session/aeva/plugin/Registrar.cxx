//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/session/aeva/plugin/Registrar.h"

#include "smtk/session/aeva/plugin/EditFreeformAttributesView.h"

namespace smtk
{
namespace session
{
namespace aeva
{
namespace plugin
{

namespace
{
using ViewWidgetList = std::tuple<EditFreeformAttributesView>;
}

void Registrar::registerTo(const smtk::view::Manager::Ptr& viewManager)
{
  viewManager->viewWidgetFactory().registerTypes<ViewWidgetList>();
}

void Registrar::unregisterFrom(const smtk::view::Manager::Ptr& viewManager)
{
  viewManager->viewWidgetFactory().unregisterTypes<ViewWidgetList>();
}

}
}
}
}
