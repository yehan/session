//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/plugin/pqFeatureReaction.h"

#include "smtk/view/Configuration.h"

#include "smtk/extension/qt/qtOperationView.h"
#include "smtk/extension/qt/qtUIManager.h"

#include "smtk/operation/Manager.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/attribute/json/jsonAttribute.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKOperationPanel.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"

#include "pqActiveObjects.h"
#include "pqApplicationCore.h"

#include <QDialog>
#include <QPushButton>

#include <iostream>

pqFeatureReactionBase::pqFeatureReactionBase(QAction* parent, bool immediate)
  : Superclass(parent)
  , m_immediate(immediate)
{
  if (!parent)
  {
    smtkErrorMacro(smtk::io::Logger::instance(), "No action.");
    return;
  }
  QObject::connect(pqSMTKBehavior::instance(),
    SIGNAL(addedManagerOnServer(pqSMTKWrapper*, pqServer*)),
    this,
    SLOT(onWrapperAdded(pqSMTKWrapper*, pqServer*)));
  // Can't call this now... our child class's virtual method will be called by onWrapperAdded.
  // Wait until the event loop starts.
  QTimer::singleShot(150, [this]() {
    pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->builtinOrActiveWrapper();
    if (wrapper)
    {
      this->onWrapperAdded(wrapper, nullptr);
    }
  });
}

void pqFeatureReactionBase::onWrapperAdded(pqSMTKWrapper* wrapper, pqServer* /*server*/)
{
  using smtk::extension::SVGIconEngine;

  auto svg = this->svg(wrapper->smtkViewManager());
  QIcon icon(new SVGIconEngine(svg));
  this->parentAction()->setIcon(icon);
}

void pqFeatureReactionBase::onTriggered()
{
  // std::cout << "Run normal feature create. Or pop up dialog.\n";
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);

  // Access the unique name associated with the operation.
  auto operationManager = wrapper->smtkOperationManager();
  auto featureOp = this->operation(operationManager);
  if (!featureOp)
  {
    smtkErrorMacro(smtk::io::Logger::instance(), "Unable to create operation.");
    return;
  }
  auto selection = wrapper->smtkSelection();
  int primaryValue = selection->selectionValueFromLabel("selected");
  std::set<smtk::resource::PersistentObject::Ptr> selected;
  selection->currentSelectionByValue(selected, primaryValue, false);
  featureOp->parameters()->associations()->setValues(selected.begin(), selected.end());

  if (m_immediate && featureOp->ableToOperate())
  {
    operationManager->launchers()(featureOp);
  }
  else
  {
    auto* opPanel = dynamic_cast<pqSMTKOperationPanel*>(
      pqApplicationCore::instance()->manager("smtk operation panel"));
    if (opPanel)
    {
      if (opPanel->editOperation(featureOp))
      {
        opPanel->show();
        opPanel->activateWindow();
        opPanel->raise();
      }
    }
  }
}
