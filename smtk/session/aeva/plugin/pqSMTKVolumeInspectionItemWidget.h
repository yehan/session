//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_aeva_pqSMTKVolumeInspectionItemWidget_h
#define smtk_session_aeva_pqSMTKVolumeInspectionItemWidget_h

#include "smtk/extension/paraview/widgets/pqSMTKAttributeItemWidget.h"

#include "vtkNew.h"

class vtkSMTransferFunctionManager;

class pqSMTKVolumeInspectionItemWidget : public pqSMTKAttributeItemWidget
{
  Q_OBJECT
public:
  pqSMTKVolumeInspectionItemWidget(const smtk::extension::qtAttributeItemInfo& info,
    Qt::Orientation orient = Qt::Horizontal);
  virtual ~pqSMTKVolumeInspectionItemWidget();

  /// Create an instance of the widget that allows users to define a sphere.
  static qtItem* createVolumeInspectionItemWidget(const qtAttributeItemInfo& info);

  bool createProxyAndWidget(vtkSMProxy*& proxy, pqInteractivePropertyWidget*& widget) override;

protected slots:
  /// Retrieve property values from ParaView proxy and store them in the attribute's Item.
  void updateItemFromWidgetInternal() override;
  /// Retrieve property values from the attribute's Item and update the ParaView proxy.
  void updateWidgetFromItemInternal() override;

protected:
  bool fetchVolumeInspectionItems(std::vector<smtk::attribute::DoubleItemPtr>& items);

  vtkNew<vtkSMTransferFunctionManager> TransferFunctions;
};

#endif //smtk_session_aeva_pqSMTKPropEditItemWidget_h
