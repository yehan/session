//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/plugin/pqAEVAShortcuts.h"

#include "smtk/session/aeva/plugin/pqAEVASurfaceFeatureToolBar.h"

// #include "smtk/session/aeva/pqProximityFeatureReaction.h"
#include "smtk/session/aeva/CellSelection.h"
#include "smtk/session/aeva/Resource.h"
#include "smtk/session/aeva/Session.h"
#include "smtk/session/aeva/operators/Duplicate.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMSMTKWrapperProxy.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/operation/Manager.h"

// ParaView
#include "pqCoreUtilities.h"

// VTK
#include "vtkDataSet.h"

// Qt
#include <QShortcut>

static pqAEVAShortcuts* s_surfaceFeatureShortcuts = nullptr;

class pqAEVAShortcuts::pqInternal
{
  pqInternal(pqAEVAShortcuts* /*q*/) {}

  ~pqInternal() = default;

protected:
  friend class pqAEVAShortcuts;

  QPointer<QShortcut> m_deselectShortcut;
  QPointer<QShortcut> m_duplicateShortcut;
};

pqAEVAShortcuts::pqAEVAShortcuts(QObject* parent)
  : Superclass(parent)
{
  m_p = new pqInternal(this);

  auto* mainWidget = pqCoreUtilities::mainWidget();

  // Called to empty the selection.
  m_p->m_deselectShortcut = new QShortcut(
    QKeySequence("Shift+Ctrl+A"), mainWidget, nullptr, nullptr, Qt::WidgetWithChildrenShortcut);
  QObject::connect(m_p->m_deselectShortcut, &QShortcut::activated, this, []() {
    pqAEVASurfaceFeatureToolBar::activateDeselect();
  });

  // Called to freeze or duplicate the current SMTK selection.
  m_p->m_duplicateShortcut = new QShortcut(
    QKeySequence("Ctrl+D"), mainWidget, nullptr, nullptr, Qt::WidgetWithChildrenShortcut);
  QObject::connect(m_p->m_duplicateShortcut, &QShortcut::activated, this, []() {
    pqAEVASurfaceFeatureToolBar* toolbar = pqAEVASurfaceFeatureToolBar::instance();
    toolbar->activateDuplicate();
  });

  if (!s_surfaceFeatureShortcuts)
  {
    s_surfaceFeatureShortcuts = this;
  }
}

pqAEVAShortcuts::~pqAEVAShortcuts()
{
  delete m_p;
  if (s_surfaceFeatureShortcuts == this)
  {
    s_surfaceFeatureShortcuts = nullptr;
  }
}

pqAEVAShortcuts* pqAEVAShortcuts::instance()
{
  return s_surfaceFeatureShortcuts;
}
