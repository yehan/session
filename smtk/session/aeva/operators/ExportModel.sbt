<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA "Export" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="export" Label="Export Resource" BaseType="operation">
      <AssociationsDef Name="Model(s)" NumberOfRequiredValues="1" Extensible="true">
        <!-- TODO should include new property filter string for "aeva_datatype" -->
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="model"/></Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="filename" NumberOfRequiredValues="1" Extensible="true"
          ShouldExist="false"
          FileFilters="VTK Image Data (*.vti);; Salome Med Mesh (*.med)">
        </File>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(export)" BaseType="result">
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
