//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_aeva_ExportFebModel_h
#define __smtk_session_aeva_ExportFebModel_h

#include "smtk/session/aeva/operators/ExportFeb.h"

namespace smtk
{
namespace session
{
namespace aeva
{

class SMTKAEVASESSION_EXPORT ExportFebModel : public ExportFeb
{
public:
  smtkTypeMacro(smtk::session::aeva::ExportFebModel);
  smtkCreateMacro(ExportFebModel);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(ExportFeb);

protected:
  virtual const char* xmlDescription() const override;
};

}
}
}

#endif
