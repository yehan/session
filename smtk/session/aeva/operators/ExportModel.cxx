//=========================================================================
//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/operators/ExportModel.h"

#include "smtk/session/aeva/ExportModel_xml.h"

namespace smtk
{
namespace session
{
namespace aeva
{

const char* ExportModel::xmlDescription() const
{
  return ExportModel_xml;
}

}
}
}
