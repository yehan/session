//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_aeva_Write_h
#define smtk_session_aeva_Write_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"
#include "smtk/session/aeva/Session.h"

class vtkXMLUnstructuredGridWriter;

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief Write an aeva resource.
  */
class SMTKAEVASESSION_EXPORT Write : public Operation
{
public:
  smtkTypeMacro(smtk::session::aeva::Write);
  smtkCreateMacro(Write);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
  void markModifiedResources(Result&) override;

private:
  bool writePoly(const smtk::model::EntityRef& cell,
    const smtk::session::aeva::Session::Ptr& session,
    vtkXMLUnstructuredGridWriter* uwriter,
    const std::string& fileDirectory,
    json& preservedUUIDs);
};

SMTKAEVASESSION_EXPORT bool write(const smtk::resource::ResourcePtr&);

}
}
}

#endif
