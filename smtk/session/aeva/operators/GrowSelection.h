//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_GrowSelection_h
#define smtk_session_aeva_GrowSelection_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/// Grow a pre-existing primitive selection by dihedral angle.
class SMTKAEVASESSION_EXPORT GrowSelection : public Operation
{

public:
  smtkTypeMacro(smtk::session::aeva::GrowSelection);
  smtkCreateMacro(GrowSelection);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

  bool ableToOperate() override;

  static bool growFromSideSetSeed(const smtk::model::Entity::Ptr& seed,
    double angleTol,
    Operation::Result& result,
    smtk::io::Logger& log);

protected:
  static bool findSeedsInPrimaries(const vtkSmartPointer<vtkDataObject>& seedData,
    std::set<smtk::model::Entity*>& primaries,
    std::map<smtk::model::Entity*, std::set<vtkIdType> >& seedMap,
    int& entityType,
    smtk::io::Logger& log);

  static vtkSmartPointer<vtkDataObject> growOnPrimary(const vtkSmartPointer<vtkDataSet>& primary,
    const std::set<vtkIdType>& seeds,
    double angleTol);

  static vtkSmartPointer<vtkDataObject> growFromSeeds(vtkSmartPointer<vtkDataObject> seedData,
    std::map<smtk::model::Entity*, std::set<vtkIdType> >& seedMap,
    double angleTol);

  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif // smtk_session_aeva_GrowSelection_h
