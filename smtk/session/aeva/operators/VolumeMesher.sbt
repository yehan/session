<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA session "volune mesher" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="volume mesher" Label="Volume Mesher" BaseType="operation">
      <BriefDescription>Create a volumetric mesh from an input surface mesh with vtk NetGenMeshing filter.</BriefDescription>
      <DetailedDescription>
        Create a volumetric mesh from an input surface mesh with vtk NetGenMeshing filter.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input surface to mesh</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Double Name="max global mesh size" Label="Max Global Mesh Size">
          <BriefDescription>Maximum global mesh size allowed</BriefDescription>
          <DefaultValue>1.0e6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>

        <Double Name="mesh density" Label="Mesh Density">
          <BriefDescription>Mesh density: 0...1 (0 => coarse; 1 => fine)</BriefDescription>
          <DefaultValue>0.5</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>

        <Int Name="element order" Label="Order of Elements">
          <BriefDescription>Generate first-order or second-order elements</BriefDescription>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
            <Max Inclusive="true">2</Max>
          </RangeInfo>
        </Int>

        <Int Name="optimize step number" Label="Number of Optimize Steps">
          <BriefDescription>Number of optimize steps to use for 3-D mesh optimization</BriefDescription>
          <DefaultValue>3</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(volume mesher)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
