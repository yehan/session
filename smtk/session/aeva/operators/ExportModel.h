//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_aeva_ExportModel_h
#define __smtk_session_aeva_ExportModel_h

#include "smtk/session/aeva/operators/Export.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/// Variant of Export that accepts models instead of resources.
class SMTKAEVASESSION_EXPORT ExportModel : public Export
{
public:
  smtkTypeMacro(smtk::session::aeva::ExportModel);
  smtkCreateMacro(ExportModel);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Export);

protected:
  virtual const char* xmlDescription() const override;
};

}
}
}

#endif
